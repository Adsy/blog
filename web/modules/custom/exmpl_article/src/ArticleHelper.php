<?php

namespace Drupal\exmpl_article;

class ArticleHelper {

  const BUNDLE = 'article';

  const TAG_FIELD_NAME = 'field_tags';

  const DRUPAL_TERM_NAME = 'Drupal';

}